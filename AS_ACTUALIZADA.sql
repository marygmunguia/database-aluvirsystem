-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.33 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para aluvir
DROP DATABASE IF EXISTS `aluvir`;
CREATE DATABASE IF NOT EXISTS `aluvir` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci */;
USE `aluvir`;

-- Volcando estructura para tabla aluvir.as_acciones
DROP TABLE IF EXISTS `as_acciones`;
CREATE TABLE IF NOT EXISTS `as_acciones` (
  `codigo_accion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_accion` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `prioridad` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`codigo_accion`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_acciones: ~9 rows (aproximadamente)
DELETE FROM `as_acciones`;
/*!40000 ALTER TABLE `as_acciones` DISABLE KEYS */;
INSERT INTO `as_acciones` (`codigo_accion`, `nombre_accion`, `prioridad`) VALUES
	(1, 'consultar', 2);
INSERT INTO `as_acciones` (`codigo_accion`, `nombre_accion`, `prioridad`) VALUES
	(2, 'crear', 1);
INSERT INTO `as_acciones` (`codigo_accion`, `nombre_accion`, `prioridad`) VALUES
	(3, 'editar', 1);
INSERT INTO `as_acciones` (`codigo_accion`, `nombre_accion`, `prioridad`) VALUES
	(4, 'eliminar', 1);
INSERT INTO `as_acciones` (`codigo_accion`, `nombre_accion`, `prioridad`) VALUES
	(5, 'reportes', 1);
INSERT INTO `as_acciones` (`codigo_accion`, `nombre_accion`, `prioridad`) VALUES
	(6, 'facturar', 1);
INSERT INTO `as_acciones` (`codigo_accion`, `nombre_accion`, `prioridad`) VALUES
	(7, 'validar', 1);
INSERT INTO `as_acciones` (`codigo_accion`, `nombre_accion`, `prioridad`) VALUES
	(8, 'cancelar', 1);
INSERT INTO `as_acciones` (`codigo_accion`, `nombre_accion`, `prioridad`) VALUES
	(9, 'imprimir', 1);
INSERT INTO `as_acciones` (`codigo_accion`, `nombre_accion`, `prioridad`) VALUES
	(10, 'realizar', 1);
/*!40000 ALTER TABLE `as_acciones` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_bitacora
DROP TABLE IF EXISTS `as_bitacora`;
CREATE TABLE IF NOT EXISTS `as_bitacora` (
  `identificador` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_registro` int(11) NOT NULL,
  `tabla_registro` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `accion_registro` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `campo_registro` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `valor_anterior` text COLLATE utf8_spanish2_ci,
  `valor_actual` text COLLATE utf8_spanish2_ci,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `codigo_usuario` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`identificador`),
  KEY `fk_usuario_bitacora` (`codigo_usuario`),
  CONSTRAINT `fk_usuario_bitacora` FOREIGN KEY (`codigo_usuario`) REFERENCES `as_usuarios` (`codigo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_bitacora: ~1 rows (aproximadamente)
DELETE FROM `as_bitacora`;
/*!40000 ALTER TABLE `as_bitacora` DISABLE KEYS */;
INSERT INTO `as_bitacora` (`identificador`, `codigo_registro`, `tabla_registro`, `accion_registro`, `campo_registro`, `valor_anterior`, `valor_actual`, `fecha`, `hora`, `codigo_usuario`) VALUES
	(2, 17, 'AS_USUARIOS', 'ACTUALIZAR', 'rol_usuario', '1', '2', '2022-06-12', '08:20:09', 18);
INSERT INTO `as_bitacora` (`identificador`, `codigo_registro`, `tabla_registro`, `accion_registro`, `campo_registro`, `valor_anterior`, `valor_actual`, `fecha`, `hora`, `codigo_usuario`) VALUES
	(3, 22, 'AS_USUARIOS', 'GUARDAR', NULL, NULL, NULL, '2022-06-12', '12:22:23', 17);
/*!40000 ALTER TABLE `as_bitacora` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_categorias
DROP TABLE IF EXISTS `as_categorias`;
CREATE TABLE IF NOT EXISTS `as_categorias` (
  `codigo_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion_categoria` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `status_categoria` int(1) DEFAULT '0',
  PRIMARY KEY (`codigo_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_categorias: ~4 rows (aproximadamente)
DELETE FROM `as_categorias`;
/*!40000 ALTER TABLE `as_categorias` DISABLE KEYS */;
INSERT INTO `as_categorias` (`codigo_categoria`, `nombre_categoria`, `descripcion_categoria`, `status_categoria`) VALUES
	(1, 'MADERA', 'ES UN MATERIAL ORTÓTROPO ENCONTRADO COMO PRINCIPAL CONTENIDO DEL TRONCO DE UN ÁRBOL. LOS ÁRBOLES SE CARACTERIZAN POR TENER TRONCOS QUE CRECEN CADA AÑO Y QUE ESTÁN COMPUESTOS POR FIBRAS DE CELULOSA UNIDAS CON LIGNINA.', 0);
INSERT INTO `as_categorias` (`codigo_categoria`, `nombre_categoria`, `descripcion_categoria`, `status_categoria`) VALUES
	(2, 'VIDRIO', 'EL VIDRIO ES UN MATERIAL INORGÁNICO DURO, FRÁGIL, TRANSPARENTE Y AMORFO QUE SE ENCUENTRA EN LA NATURALEZA, AUNQUE TAMBIÉN PUEDE SER PRODUCIDO POR EL SER HUMANO.', 0);
INSERT INTO `as_categorias` (`codigo_categoria`, `nombre_categoria`, `descripcion_categoria`, `status_categoria`) VALUES
	(3, 'PLASTICO', 'SON MATERIALES DE ORIGEN ORGÁNICO Y DE ELEVADO PESO MOLECULAR, CONSTITUIDOS POR LARGAS CADENAS DE MOLÉCULAS LLAMADAS POLÍMEROS. SE OBTIENEN PRINCIPALMENTE A PARTIR DEL PETRÓLEO Y DEL GAS NATURAL.', 0);
INSERT INTO `as_categorias` (`codigo_categoria`, `nombre_categoria`, `descripcion_categoria`, `status_categoria`) VALUES
	(4, 'METALES', 'SON SUSTANCIAS ELEMENTALES, COMO POR EJEMPLO, EL ORO, LA PLATA Y EL COBRE. SON CRISTALINOS EN SU FORMA SÓLIDA Y SE ENCUENTRAN DE MANERA NATURAL EN LOS MINERALES.', 0);
/*!40000 ALTER TABLE `as_categorias` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_clientes
DROP TABLE IF EXISTS `as_clientes`;
CREATE TABLE IF NOT EXISTS `as_clientes` (
  `codigo_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_identificacion` int(11) DEFAULT NULL,
  `identificacion` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `sexo` enum('FEMENINO','MASCULINO','NO DEFINIDO') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `celular` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `credito` int(1) NOT NULL DEFAULT '0',
  `limite_credito` double(17,2) DEFAULT '0.00',
  PRIMARY KEY (`codigo_cliente`),
  KEY `fk_cliente_documento` (`tipo_identificacion`),
  CONSTRAINT `fk_cliente_documento` FOREIGN KEY (`tipo_identificacion`) REFERENCES `as_tipo_documento` (`codigo_documento`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_clientes: ~2 rows (aproximadamente)
DELETE FROM `as_clientes`;
/*!40000 ALTER TABLE `as_clientes` DISABLE KEYS */;
INSERT INTO `as_clientes` (`codigo_cliente`, `tipo_identificacion`, `identificacion`, `nombre`, `apellido`, `fecha_nacimiento`, `sexo`, `email`, `telefono`, `celular`, `credito`, `limite_credito`) VALUES
	(1, 1, '1805198563952', 'LUZ MARIA', 'GOMÉZ', '1987-06-08', 'FEMENINO', 'luzmaria@gmail.com', '', '88565623', 1, 0.00);
INSERT INTO `as_clientes` (`codigo_cliente`, `tipo_identificacion`, `identificacion`, `nombre`, `apellido`, `fecha_nacimiento`, `sexo`, `email`, `telefono`, `celular`, `credito`, `limite_credito`) VALUES
	(7, 1, '1807200000756', 'MARIA JHOSE', 'GARCIA MUNGUIA', '2000-01-24', 'FEMENINO', 'mariajhosegarcia@gmail.com', '24469565', '88409092', 0, 15000.00);
INSERT INTO `as_clientes` (`codigo_cliente`, `tipo_identificacion`, `identificacion`, `nombre`, `apellido`, `fecha_nacimiento`, `sexo`, `email`, `telefono`, `celular`, `credito`, `limite_credito`) VALUES
	(9, 1, '1807198545632', 'JUANA MARIA', 'ROSALES GOMÉZ', '2000-02-28', 'FEMENINO', 'juanamariarosales@gmail.com', '24469589', '88565623', 1, 0.00);
/*!40000 ALTER TABLE `as_clientes` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_compras
DROP TABLE IF EXISTS `as_compras`;
CREATE TABLE IF NOT EXISTS `as_compras` (
  `codigo_compra` int(11) NOT NULL AUTO_INCREMENT,
  `no_factura` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `proveedor` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `subtotal` decimal(20,2) NOT NULL,
  `impuestos` decimal(20,2) NOT NULL,
  `descuento` decimal(20,2) DEFAULT NULL,
  `total` decimal(20,2) NOT NULL,
  `materiales` text COLLATE utf8_spanish2_ci,
  PRIMARY KEY (`codigo_compra`),
  KEY `FK_as_compras_as_proveedores` (`proveedor`) USING BTREE,
  CONSTRAINT `FK_as_compras_as_proveedores` FOREIGN KEY (`proveedor`) REFERENCES `as_proveedores` (`codigo_proveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_compras: ~3 rows (aproximadamente)
DELETE FROM `as_compras`;
/*!40000 ALTER TABLE `as_compras` DISABLE KEYS */;
INSERT INTO `as_compras` (`codigo_compra`, `no_factura`, `proveedor`, `fecha`, `subtotal`, `impuestos`, `descuento`, `total`, `materiales`) VALUES
	(1, NULL, 3, '2022-07-03', 100.00, 15.00, 0.00, 115.00, NULL);
INSERT INTO `as_compras` (`codigo_compra`, `no_factura`, `proveedor`, `fecha`, `subtotal`, `impuestos`, `descuento`, `total`, `materiales`) VALUES
	(2, NULL, 7, '2022-07-03', 200.00, 30.00, 0.00, 230.00, NULL);
INSERT INTO `as_compras` (`codigo_compra`, `no_factura`, `proveedor`, `fecha`, `subtotal`, `impuestos`, `descuento`, `total`, `materiales`) VALUES
	(3, '', 3, '2022-06-27', 100.00, 15.00, 0.00, 115.00, '[{"id":"3","descripcion":"BARRAS DE HIERRO","cantidad":"1","precio":"100"}]');
/*!40000 ALTER TABLE `as_compras` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_compras_materiales
DROP TABLE IF EXISTS `as_compras_materiales`;
CREATE TABLE IF NOT EXISTS `as_compras_materiales` (
  `correlativo` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_compra` int(11) NOT NULL DEFAULT '0',
  `codigo_material` int(11) NOT NULL DEFAULT '0',
  `cantidad` int(11) NOT NULL DEFAULT '0',
  `precio` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`correlativo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_compras_materiales: ~1 rows (aproximadamente)
DELETE FROM `as_compras_materiales`;
/*!40000 ALTER TABLE `as_compras_materiales` DISABLE KEYS */;
INSERT INTO `as_compras_materiales` (`correlativo`, `codigo_compra`, `codigo_material`, `cantidad`, `precio`) VALUES
	(1, 3, 3, 1, 100);
/*!40000 ALTER TABLE `as_compras_materiales` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_direcciones
DROP TABLE IF EXISTS `as_direcciones`;
CREATE TABLE IF NOT EXISTS `as_direcciones` (
  `codigo_direccion` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_cliente` int(11) NOT NULL,
  `departamento` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `municipio` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `ciudad` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `barrio_colonia` varchar(60) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `detalle_direccion` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`codigo_direccion`),
  KEY `fk_direccion_cliente` (`codigo_cliente`),
  CONSTRAINT `fk_direccion_cliente` FOREIGN KEY (`codigo_cliente`) REFERENCES `as_clientes` (`codigo_cliente`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_direcciones: ~6 rows (aproximadamente)
DELETE FROM `as_direcciones`;
/*!40000 ALTER TABLE `as_direcciones` DISABLE KEYS */;
INSERT INTO `as_direcciones` (`codigo_direccion`, `codigo_cliente`, `departamento`, `municipio`, `ciudad`, `barrio_colonia`, `detalle_direccion`) VALUES
	(1, 1, 'YORO', 'OLANCHITO', 'OLANCHITO', 'COL. ROSALES', 'CALLE DE LA IGLESIA DE LOS MORMONES');
INSERT INTO `as_direcciones` (`codigo_direccion`, `codigo_cliente`, `departamento`, `municipio`, `ciudad`, `barrio_colonia`, `detalle_direccion`) VALUES
	(4, 7, 'FRANCISCO MORAZÁN', 'DISTRITO CENTRAL', 'TEGUCIGALPA', 'COL. KENNEDY', 'CALLE FRENTE A IGLESIA LUZ Y VERDAD');
INSERT INTO `as_direcciones` (`codigo_direccion`, `codigo_cliente`, `departamento`, `municipio`, `ciudad`, `barrio_colonia`, `detalle_direccion`) VALUES
	(5, 9, 'ATLÁNTIDA', 'LA CEIBA', 'LA CEIBA', 'LA GRAN CRUZ DE JESÚS', 'CALLE DEL GRAN BOULEVAR');
INSERT INTO `as_direcciones` (`codigo_direccion`, `codigo_cliente`, `departamento`, `municipio`, `ciudad`, `barrio_colonia`, `detalle_direccion`) VALUES
	(6, 9, 'FRANCISCO MORAZÁN', 'DISTRITO CENTRAL', 'TEGUCIGALPA', 'COL. KENNEDY', 'CALLE DE CARNITAS KENNEDY');
INSERT INTO `as_direcciones` (`codigo_direccion`, `codigo_cliente`, `departamento`, `municipio`, `ciudad`, `barrio_colonia`, `detalle_direccion`) VALUES
	(7, 1, 'ATLÁNTIDA', 'LA CEIBA', 'LA CEIBA', 'LA GRAN CRUZ DE JESÚS', 'CALLE PRINCIPAL SALIDA A LA GRAN MURALLA');
/*!40000 ALTER TABLE `as_direcciones` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_empleados
DROP TABLE IF EXISTS `as_empleados`;
CREATE TABLE IF NOT EXISTS `as_empleados` (
  `codigo_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_documento` int(11) NOT NULL,
  `no_documento` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `sexo` enum('FEMENINO','MASCULINO','NO DEFINIDO') COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `celular` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`codigo_empleado`),
  KEY `FK_as_empleados_as_tipo_documento` (`tipo_documento`),
  CONSTRAINT `FK_as_empleados_as_tipo_documento` FOREIGN KEY (`tipo_documento`) REFERENCES `as_tipo_documento` (`codigo_documento`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_empleados: ~2 rows (aproximadamente)
DELETE FROM `as_empleados`;
/*!40000 ALTER TABLE `as_empleados` DISABLE KEYS */;
INSERT INTO `as_empleados` (`codigo_empleado`, `tipo_documento`, `no_documento`, `nombre`, `apellido`, `fecha_nacimiento`, `sexo`, `email`, `telefono`, `celular`) VALUES
	(1, 1, '', 'JUAN', 'PONCE', '1997-07-09', 'MASCULINO', '', '', '88563210');
INSERT INTO `as_empleados` (`codigo_empleado`, `tipo_documento`, `no_documento`, `nombre`, `apellido`, `fecha_nacimiento`, `sexo`, `email`, `telefono`, `celular`) VALUES
	(2, 4, '', 'TANIA', 'ROBLES PONCE', '2022-06-26', 'FEMENINO', '', '', '');
/*!40000 ALTER TABLE `as_empleados` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_impuestos_sobre_venta
DROP TABLE IF EXISTS `as_impuestos_sobre_venta`;
CREATE TABLE IF NOT EXISTS `as_impuestos_sobre_venta` (
  `codigo_impuesto` int(11) NOT NULL AUTO_INCREMENT,
  `valor_impuesto` decimal(5,2) NOT NULL,
  `nombre_impuesto` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`codigo_impuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_impuestos_sobre_venta: ~0 rows (aproximadamente)
DELETE FROM `as_impuestos_sobre_venta`;
/*!40000 ALTER TABLE `as_impuestos_sobre_venta` DISABLE KEYS */;
/*!40000 ALTER TABLE `as_impuestos_sobre_venta` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_info_empresa
DROP TABLE IF EXISTS `as_info_empresa`;
CREATE TABLE IF NOT EXISTS `as_info_empresa` (
  `cod_empresa` int(1) NOT NULL DEFAULT '1',
  `rtn_empresa` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre_empresa` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `propietario_empresa` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion_empresa` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono_empresa` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `celular_empresa` varchar(15) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '',
  `email_empresa` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email_soporte` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='TABLA CON TODA LA INFORMACION DE LA EMPRESA';

-- Volcando datos para la tabla aluvir.as_info_empresa: ~1 rows (aproximadamente)
DELETE FROM `as_info_empresa`;
/*!40000 ALTER TABLE `as_info_empresa` DISABLE KEYS */;
INSERT INTO `as_info_empresa` (`cod_empresa`, `rtn_empresa`, `nombre_empresa`, `propietario_empresa`, `direccion_empresa`, `telefono_empresa`, `celular_empresa`, `email_empresa`, `email_soporte`) VALUES
	(1, '01081995003526', 'Aluvir', 'Daniel Medina', 'Col. Gran corazón de Jesús', '24468592', '89653110', 'aluvircontratistas@gmail.com', 'info@aluvirsystem.com');
/*!40000 ALTER TABLE `as_info_empresa` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_materiales
DROP TABLE IF EXISTS `as_materiales`;
CREATE TABLE IF NOT EXISTS `as_materiales` (
  `codigo_material` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_barras` varchar(30) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_material` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `detalle_material` text COLLATE utf8_spanish2_ci,
  `categoria` int(11) NOT NULL,
  `precio_venta` decimal(20,2) unsigned NOT NULL,
  `existencias` int(11) NOT NULL,
  `existencias_minimas` int(11) NOT NULL,
  `existencias_maximas` int(11) NOT NULL,
  PRIMARY KEY (`codigo_material`),
  KEY `fk_categorias_materiales` (`categoria`),
  CONSTRAINT `fk_categorias_materiales` FOREIGN KEY (`categoria`) REFERENCES `as_categorias` (`codigo_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_materiales: ~4 rows (aproximadamente)
DELETE FROM `as_materiales`;
/*!40000 ALTER TABLE `as_materiales` DISABLE KEYS */;
INSERT INTO `as_materiales` (`codigo_material`, `codigo_barras`, `nombre_material`, `detalle_material`, `categoria`, `precio_venta`, `existencias`, `existencias_minimas`, `existencias_maximas`) VALUES
	(1, '1785369874', 'CAOBA', '100% IMPORTADA', 1, 350.00, 500, 15, 2000);
INSERT INTO `as_materiales` (`codigo_material`, `codigo_barras`, `nombre_material`, `detalle_material`, `categoria`, `precio_venta`, `existencias`, `existencias_minimas`, `existencias_maximas`) VALUES
	(2, '2735737537', 'VIDRIO A PRUEBA DE BALAS', NULL, 2, 1500.00, 200, 20, 1000);
INSERT INTO `as_materiales` (`codigo_material`, `codigo_barras`, `nombre_material`, `detalle_material`, `categoria`, `precio_venta`, `existencias`, `existencias_minimas`, `existencias_maximas`) VALUES
	(3, '7375289415', 'BARRAS DE HIERRO', NULL, 4, 200.00, 120, 10, 200);
INSERT INTO `as_materiales` (`codigo_material`, `codigo_barras`, `nombre_material`, `detalle_material`, `categoria`, `precio_venta`, `existencias`, `existencias_minimas`, `existencias_maximas`) VALUES
	(4, '1581265255', 'TUBO DE PVC', '', 1, 26.25, 100, 15, 2000);
/*!40000 ALTER TABLE `as_materiales` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_metodos_de_pago
DROP TABLE IF EXISTS `as_metodos_de_pago`;
CREATE TABLE IF NOT EXISTS `as_metodos_de_pago` (
  `codigo_metodo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_metodo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion_metodo` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `status_metodo` int(1) NOT NULL DEFAULT '0',
  `comprobante` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo_metodo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_metodos_de_pago: ~3 rows (aproximadamente)
DELETE FROM `as_metodos_de_pago`;
/*!40000 ALTER TABLE `as_metodos_de_pago` DISABLE KEYS */;
INSERT INTO `as_metodos_de_pago` (`codigo_metodo`, `nombre_metodo`, `descripcion_metodo`, `status_metodo`, `comprobante`) VALUES
	(1, 'EFECTIVO', NULL, 0, 1);
INSERT INTO `as_metodos_de_pago` (`codigo_metodo`, `nombre_metodo`, `descripcion_metodo`, `status_metodo`, `comprobante`) VALUES
	(2, 'TARJETA DE CRÉDITO', NULL, 0, 0);
INSERT INTO `as_metodos_de_pago` (`codigo_metodo`, `nombre_metodo`, `descripcion_metodo`, `status_metodo`, `comprobante`) VALUES
	(3, 'TARJETA DE DÉBITO', NULL, 0, 0);
INSERT INTO `as_metodos_de_pago` (`codigo_metodo`, `nombre_metodo`, `descripcion_metodo`, `status_metodo`, `comprobante`) VALUES
	(4, 'TRANSFERENCIA BANCARIA', NULL, 0, 0);
INSERT INTO `as_metodos_de_pago` (`codigo_metodo`, `nombre_metodo`, `descripcion_metodo`, `status_metodo`, `comprobante`) VALUES
	(5, 'PAYPAL', '', 0, 0);
/*!40000 ALTER TABLE `as_metodos_de_pago` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_parametros_fiscales
DROP TABLE IF EXISTS `as_parametros_fiscales`;
CREATE TABLE IF NOT EXISTS `as_parametros_fiscales` (
  `codigo` int(11) DEFAULT NULL,
  `cai` varchar(40) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `punto_emision` varchar(3) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `establecimiento` varchar(3) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `documento_fiscal` varchar(2) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `min_correlativo` int(8) unsigned zerofill DEFAULT NULL,
  `max_correlativo` int(8) unsigned zerofill DEFAULT NULL,
  `fecha_limite_emision` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_parametros_fiscales: ~0 rows (aproximadamente)
DELETE FROM `as_parametros_fiscales`;
/*!40000 ALTER TABLE `as_parametros_fiscales` DISABLE KEYS */;
/*!40000 ALTER TABLE `as_parametros_fiscales` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_parametros_seguridad
DROP TABLE IF EXISTS `as_parametros_seguridad`;
CREATE TABLE IF NOT EXISTS `as_parametros_seguridad` (
  `codigo_seguridad` int(1) NOT NULL AUTO_INCREMENT,
  `codigo_empresa` int(11) NOT NULL DEFAULT '1',
  `max_password` int(2) NOT NULL DEFAULT '16',
  `max_intentos` int(2) NOT NULL DEFAULT '5',
  `minutos_vencimiento` int(2) NOT NULL DEFAULT '30',
  `preguntas_necesarias` int(1) NOT NULL DEFAULT '2',
  `dias_pass_vencida` int(2) NOT NULL DEFAULT '30',
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`codigo_seguridad`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_parametros_seguridad: ~1 rows (aproximadamente)
DELETE FROM `as_parametros_seguridad`;
/*!40000 ALTER TABLE `as_parametros_seguridad` DISABLE KEYS */;
INSERT INTO `as_parametros_seguridad` (`codigo_seguridad`, `codigo_empresa`, `max_password`, `max_intentos`, `minutos_vencimiento`, `preguntas_necesarias`, `dias_pass_vencida`, `status`) VALUES
	(1, 1, 16, 5, 30, 2, 30, 0);
/*!40000 ALTER TABLE `as_parametros_seguridad` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_permisos
DROP TABLE IF EXISTS `as_permisos`;
CREATE TABLE IF NOT EXISTS `as_permisos` (
  `codigo_permiso` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_accion` int(11) NOT NULL,
  `codigo_seccion` int(11) NOT NULL,
  PRIMARY KEY (`codigo_permiso`),
  KEY `fk_accion` (`codigo_accion`),
  KEY `fk_secciones` (`codigo_seccion`),
  CONSTRAINT `fk_accion` FOREIGN KEY (`codigo_accion`) REFERENCES `as_acciones` (`codigo_accion`),
  CONSTRAINT `fk_secciones` FOREIGN KEY (`codigo_seccion`) REFERENCES `as_secciones` (`codigo_seccion`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_permisos: ~72 rows (aproximadamente)
DELETE FROM `as_permisos`;
/*!40000 ALTER TABLE `as_permisos` DISABLE KEYS */;
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(1, 1, 145);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(2, 2, 145);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(3, 3, 145);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(4, 1, 100);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(5, 3, 100);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(6, 2, 100);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(7, 3, 160);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(8, 1, 101);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(9, 2, 101);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(10, 3, 101);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(11, 4, 101);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(13, 4, 145);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(14, 4, 100);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(15, 1, 105);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(16, 2, 105);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(17, 3, 105);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(18, 4, 105);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(19, 1, 125);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(20, 8, 125);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(21, 9, 125);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(22, 7, 125);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(23, 1, 107);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(24, 2, 107);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(25, 1, 150);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(26, 2, 150);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(27, 3, 150);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(28, 4, 150);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(29, 1, 106);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(30, 2, 106);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(31, 3, 106);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(32, 4, 106);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(33, 3, 107);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(34, 4, 107);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(35, 1, 110);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(36, 2, 110);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(37, 3, 110);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(38, 4, 110);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(39, 1, 111);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(40, 2, 111);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(41, 3, 111);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(42, 4, 111);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(43, 1, 115);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(44, 2, 115);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(45, 3, 115);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(46, 4, 115);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(47, 9, 115);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(48, 1, 120);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(49, 2, 120);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(50, 3, 120);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(51, 4, 120);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(52, 9, 120);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(53, 8, 120);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(54, 1, 130);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(55, 2, 130);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(56, 3, 130);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(57, 4, 130);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(58, 2, 125);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(59, 3, 125);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(60, 4, 125);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(61, 1, 135);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(62, 2, 135);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(63, 3, 135);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(64, 4, 135);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(65, 3, 165);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(66, 3, 170);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(67, 1, 140);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(68, 2, 140);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(69, 3, 140);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(70, 4, 140);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(71, 10, 156);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(72, 2, 155);
INSERT INTO `as_permisos` (`codigo_permiso`, `codigo_accion`, `codigo_seccion`) VALUES
	(73, 1, 180);
/*!40000 ALTER TABLE `as_permisos` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_preguntas_seguridad
DROP TABLE IF EXISTS `as_preguntas_seguridad`;
CREATE TABLE IF NOT EXISTS `as_preguntas_seguridad` (
  `codigo_pregunta` int(11) NOT NULL AUTO_INCREMENT,
  `cuerpo_pregunta` varchar(255) COLLATE utf8_spanish2_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo_pregunta`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_preguntas_seguridad: ~5 rows (aproximadamente)
DELETE FROM `as_preguntas_seguridad`;
/*!40000 ALTER TABLE `as_preguntas_seguridad` DISABLE KEYS */;
INSERT INTO `as_preguntas_seguridad` (`codigo_pregunta`, `cuerpo_pregunta`) VALUES
	(1, '¿cual fue el nombre de tu primer mascota?');
INSERT INTO `as_preguntas_seguridad` (`codigo_pregunta`, `cuerpo_pregunta`) VALUES
	(2, '¿donde se conocieron tus padres?');
INSERT INTO `as_preguntas_seguridad` (`codigo_pregunta`, `cuerpo_pregunta`) VALUES
	(3, '¿cual es el primer nombre de tu abuelo?');
INSERT INTO `as_preguntas_seguridad` (`codigo_pregunta`, `cuerpo_pregunta`) VALUES
	(4, '¿cual fue el nombre de tu maestra en segundo grado?');
INSERT INTO `as_preguntas_seguridad` (`codigo_pregunta`, `cuerpo_pregunta`) VALUES
	(5, '¿en que hospital nacio tu padre?');
/*!40000 ALTER TABLE `as_preguntas_seguridad` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_productos
DROP TABLE IF EXISTS `as_productos`;
CREATE TABLE IF NOT EXISTS `as_productos` (
  `codigo_producto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_producto` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `detalle_producto` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `categoria` int(11) NOT NULL,
  `precio_venta` decimal(20,2) NOT NULL,
  `existencias_iniciales` int(11) DEFAULT '0',
  `existencias` int(11) DEFAULT '0',
  `materiales_producto` text COLLATE utf8_spanish2_ci,
  PRIMARY KEY (`codigo_producto`),
  KEY `fk_producto_categoria` (`categoria`),
  CONSTRAINT `fk_producto_categoria` FOREIGN KEY (`categoria`) REFERENCES `as_categorias` (`codigo_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_productos: ~2 rows (aproximadamente)
DELETE FROM `as_productos`;
/*!40000 ALTER TABLE `as_productos` DISABLE KEYS */;
INSERT INTO `as_productos` (`codigo_producto`, `nombre_producto`, `detalle_producto`, `categoria`, `precio_venta`, `existencias_iniciales`, `existencias`, `materiales_producto`) VALUES
	(2, 'PUERTA DE MADERA DE CAOBA', 'PUERTA A MEDIDA DE 2 METROS DE ALTO Y 0.4 METROS DE LARGO', 1, 7560.00, 10, 0, '[{"id":"2","descripcion":"VIDRIO A PRUEBA DE BALAS","cantidad":"2"},{"id":"4","descripcion":"TUBO DE PVC","cantidad":"2"}]');
INSERT INTO `as_productos` (`codigo_producto`, `nombre_producto`, `detalle_producto`, `categoria`, `precio_venta`, `existencias_iniciales`, `existencias`, `materiales_producto`) VALUES
	(3, 'VENTANA DE VIDRIO CORREDIZA', 'ALTO 1 METRO ANCHO 1.5 METROS', 2, 2500.00, 5, NULL, '[{"id":"2","descripcion":"VIDRIO A PRUEBA DE BALAS","cantidad":"1"},{"id":"4","descripcion":"TUBO DE PVC","cantidad":"2"}]');
/*!40000 ALTER TABLE `as_productos` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_productos_materiales
DROP TABLE IF EXISTS `as_productos_materiales`;
CREATE TABLE IF NOT EXISTS `as_productos_materiales` (
  `correlativo` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_producto` int(11) NOT NULL,
  `codigo_materiales` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`correlativo`),
  KEY `FK__as_productos` (`codigo_producto`),
  KEY `FK__as_materiales` (`codigo_materiales`),
  CONSTRAINT `FK__as_materiales` FOREIGN KEY (`codigo_materiales`) REFERENCES `as_materiales` (`codigo_material`),
  CONSTRAINT `FK__as_productos` FOREIGN KEY (`codigo_producto`) REFERENCES `as_productos` (`codigo_producto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_productos_materiales: ~4 rows (aproximadamente)
DELETE FROM `as_productos_materiales`;
/*!40000 ALTER TABLE `as_productos_materiales` DISABLE KEYS */;
INSERT INTO `as_productos_materiales` (`correlativo`, `codigo_producto`, `codigo_materiales`, `cantidad`) VALUES
	(3, 3, 2, 1);
INSERT INTO `as_productos_materiales` (`correlativo`, `codigo_producto`, `codigo_materiales`, `cantidad`) VALUES
	(4, 3, 4, 2);
INSERT INTO `as_productos_materiales` (`correlativo`, `codigo_producto`, `codigo_materiales`, `cantidad`) VALUES
	(9, 2, 2, 2);
INSERT INTO `as_productos_materiales` (`correlativo`, `codigo_producto`, `codigo_materiales`, `cantidad`) VALUES
	(10, 2, 4, 2);
/*!40000 ALTER TABLE `as_productos_materiales` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_proveedores
DROP TABLE IF EXISTS `as_proveedores`;
CREATE TABLE IF NOT EXISTS `as_proveedores` (
  `codigo_proveedor` int(11) NOT NULL AUTO_INCREMENT,
  `rtn_proveedor` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_proveedor` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion_proveedor` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono_proveedor` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `celular_proveedor` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email_proveedor` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre_contacto` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `celular_contacto` varchar(10) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email_contacto` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`codigo_proveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_proveedores: ~8 rows (aproximadamente)
DELETE FROM `as_proveedores`;
/*!40000 ALTER TABLE `as_proveedores` DISABLE KEYS */;
INSERT INTO `as_proveedores` (`codigo_proveedor`, `rtn_proveedor`, `nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`, `celular_proveedor`, `email_proveedor`, `nombre_contacto`, `celular_contacto`, `email_contacto`) VALUES
	(2, ' ', 'PLASTICOS INDUSTRIALES LOPEZ', ' ', '', ' ', ' ', ' ', ' ', ' ');
INSERT INTO `as_proveedores` (`codigo_proveedor`, `rtn_proveedor`, `nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`, `celular_proveedor`, `email_proveedor`, `nombre_contacto`, `celular_contacto`, `email_contacto`) VALUES
	(3, NULL, 'INVERSIONES JUAREZ', 'COL. LA GRAN SEMILLA DEL FRUTO DEL BOSQUE', '24463585', '88963520', 'info@inversionesjuarez.es', NULL, NULL, NULL);
INSERT INTO `as_proveedores` (`codigo_proveedor`, `rtn_proveedor`, `nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`, `celular_proveedor`, `email_proveedor`, `nombre_contacto`, `celular_contacto`, `email_contacto`) VALUES
	(5, '', 'VIDRIOS DEL SAN JUAN', '', '', '', '', 'JUAN LOPEZ', '', '');
INSERT INTO `as_proveedores` (`codigo_proveedor`, `rtn_proveedor`, `nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`, `celular_proveedor`, `email_proveedor`, `nombre_contacto`, `celular_contacto`, `email_contacto`) VALUES
	(6, '', 'MADERAS LA GRAN AVENIDA', '', '', '', '', 'PEDRO PONCE', NULL, '');
INSERT INTO `as_proveedores` (`codigo_proveedor`, `rtn_proveedor`, `nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`, `celular_proveedor`, `email_proveedor`, `nombre_contacto`, `celular_contacto`, `email_contacto`) VALUES
	(7, '', 'INNOVACIONES LA GRAN CRUZ DE JESUS', '', '', '', '', 'ROSA MENDEZ', '85632110', '');
INSERT INTO `as_proveedores` (`codigo_proveedor`, `rtn_proveedor`, `nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`, `celular_proveedor`, `email_proveedor`, `nombre_contacto`, `celular_contacto`, `email_contacto`) VALUES
	(8, '', 'MADERAS LA GRAN AVENIDA', '', '', '', '', 'PEDRO PONCE', '85639452', '');
INSERT INTO `as_proveedores` (`codigo_proveedor`, `rtn_proveedor`, `nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`, `celular_proveedor`, `email_proveedor`, `nombre_contacto`, `celular_contacto`, `email_contacto`) VALUES
	(9, '', 'MADERAS LA GRAN AVENIDA', '', '24469536', '98652310', '', 'PEDRO PONCE', '96321010', '');
INSERT INTO `as_proveedores` (`codigo_proveedor`, `rtn_proveedor`, `nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`, `celular_proveedor`, `email_proveedor`, `nombre_contacto`, `celular_contacto`, `email_contacto`) VALUES
	(10, '', 'VIDRIOS DEL SAN JUAN', '', '24469530', '', 'ventas@vidriosdelsanjuan.es', 'JUAN LOPEZ', '85633223', 'juan@vidriosdelsanjuan.es');
INSERT INTO `as_proveedores` (`codigo_proveedor`, `rtn_proveedor`, `nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`, `celular_proveedor`, `email_proveedor`, `nombre_contacto`, `celular_contacto`, `email_contacto`) VALUES
	(11, '', 'VIDRIOS DEL SAN JUAN', '', '', '', '', 'JUAN LOPEZ', '86932017', '');
/*!40000 ALTER TABLE `as_proveedores` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_roles
DROP TABLE IF EXISTS `as_roles`;
CREATE TABLE IF NOT EXISTS `as_roles` (
  `codigo_rol` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_rol` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion_rol` text COLLATE utf8_spanish2_ci NOT NULL,
  `status_registro` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_roles: ~7 rows (aproximadamente)
DELETE FROM `as_roles`;
/*!40000 ALTER TABLE `as_roles` DISABLE KEYS */;
INSERT INTO `as_roles` (`codigo_rol`, `nombre_rol`, `descripcion_rol`, `status_registro`) VALUES
	(1, 'administrador', 'ACCESO COMPLETO AL SISTEMA', 0);
INSERT INTO `as_roles` (`codigo_rol`, `nombre_rol`, `descripcion_rol`, `status_registro`) VALUES
	(2, 'PROPIETARIO', '', 0);
INSERT INTO `as_roles` (`codigo_rol`, `nombre_rol`, `descripcion_rol`, `status_registro`) VALUES
	(5, 'OPERARIO', '', 0);
INSERT INTO `as_roles` (`codigo_rol`, `nombre_rol`, `descripcion_rol`, `status_registro`) VALUES
	(7, 'SUPERVISOR', '', 0);
INSERT INTO `as_roles` (`codigo_rol`, `nombre_rol`, `descripcion_rol`, `status_registro`) VALUES
	(9, 'PROFESOR', '', 0);
/*!40000 ALTER TABLE `as_roles` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_roles_permiso
DROP TABLE IF EXISTS `as_roles_permiso`;
CREATE TABLE IF NOT EXISTS `as_roles_permiso` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_rol` int(11) NOT NULL,
  `codigo_permiso` int(11) NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_permiso` (`codigo_permiso`),
  KEY `fk_rol` (`codigo_rol`),
  CONSTRAINT `fk_permiso` FOREIGN KEY (`codigo_permiso`) REFERENCES `as_permisos` (`codigo_permiso`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rol` FOREIGN KEY (`codigo_rol`) REFERENCES `as_roles` (`codigo_rol`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=701 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_roles_permiso: ~79 rows (aproximadamente)
DELETE FROM `as_roles_permiso`;
/*!40000 ALTER TABLE `as_roles_permiso` DISABLE KEYS */;
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(106, 1, 4);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(107, 1, 6);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(108, 1, 5);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(109, 1, 14);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(110, 1, 1);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(111, 1, 2);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(112, 1, 3);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(113, 1, 13);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(114, 1, 25);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(115, 1, 26);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(116, 1, 27);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(117, 1, 28);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(329, 9, 4);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(330, 9, 6);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(331, 9, 5);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(332, 9, 14);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(333, 9, 8);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(334, 9, 15);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(335, 9, 16);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(336, 9, 29);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(337, 9, 30);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(338, 9, 31);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(339, 9, 33);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(340, 9, 34);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(648, 2, 4);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(649, 2, 6);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(650, 2, 5);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(651, 2, 14);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(652, 2, 8);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(653, 2, 9);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(654, 2, 10);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(655, 2, 11);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(656, 2, 15);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(657, 2, 16);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(658, 2, 17);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(659, 2, 18);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(660, 2, 29);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(661, 2, 30);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(662, 2, 31);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(663, 2, 32);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(664, 2, 23);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(665, 2, 24);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(666, 2, 33);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(667, 2, 34);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(668, 2, 35);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(669, 2, 36);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(670, 2, 37);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(671, 2, 38);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(672, 2, 39);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(673, 2, 40);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(674, 2, 43);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(675, 2, 48);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(676, 2, 19);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(677, 2, 54);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(678, 2, 55);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(679, 2, 56);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(680, 2, 57);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(681, 2, 61);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(682, 2, 62);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(683, 2, 63);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(684, 2, 64);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(685, 2, 67);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(686, 2, 68);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(687, 2, 69);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(688, 2, 70);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(689, 2, 1);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(690, 2, 2);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(691, 2, 3);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(692, 2, 13);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(693, 2, 25);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(694, 2, 26);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(695, 2, 27);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(696, 2, 28);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(697, 2, 72);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(698, 2, 71);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(699, 2, 65);
INSERT INTO `as_roles_permiso` (`codigo`, `codigo_rol`, `codigo_permiso`) VALUES
	(700, 2, 73);
/*!40000 ALTER TABLE `as_roles_permiso` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_secciones
DROP TABLE IF EXISTS `as_secciones`;
CREATE TABLE IF NOT EXISTS `as_secciones` (
  `codigo_seccion` int(11) NOT NULL,
  `nombre_seccion` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`codigo_seccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_secciones: ~19 rows (aproximadamente)
DELETE FROM `as_secciones`;
/*!40000 ALTER TABLE `as_secciones` DISABLE KEYS */;
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(100, 'clientes');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(101, 'direcciones');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(105, 'categorias');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(106, 'materiales');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(107, 'productos');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(110, 'proveedores');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(111, 'compras');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(115, 'cotizaciones');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(120, 'ordenes de trabajo');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(125, 'factura de venta');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(130, 'tipo de documento');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(135, 'métodos de pagos');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(140, 'empleados');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(145, 'usuarios');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(150, 'roles');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(155, 'backup');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(156, 'restore');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(160, 'empresa');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(165, 'facturación');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(170, 'seguridad');
INSERT INTO `as_secciones` (`codigo_seccion`, `nombre_seccion`) VALUES
	(180, 'bitacora');
/*!40000 ALTER TABLE `as_secciones` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_tipo_documento
DROP TABLE IF EXISTS `as_tipo_documento`;
CREATE TABLE IF NOT EXISTS `as_tipo_documento` (
  `codigo_documento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_documento` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion_documento` text COLLATE utf8_spanish2_ci,
  `status_documento` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigo_documento`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_tipo_documento: ~3 rows (aproximadamente)
DELETE FROM `as_tipo_documento`;
/*!40000 ALTER TABLE `as_tipo_documento` DISABLE KEYS */;
INSERT INTO `as_tipo_documento` (`codigo_documento`, `nombre_documento`, `descripcion_documento`, `status_documento`) VALUES
	(1, 'IDENTIFICACION', NULL, 0);
INSERT INTO `as_tipo_documento` (`codigo_documento`, `nombre_documento`, `descripcion_documento`, `status_documento`) VALUES
	(2, 'PASAPORTE', NULL, 0);
INSERT INTO `as_tipo_documento` (`codigo_documento`, `nombre_documento`, `descripcion_documento`, `status_documento`) VALUES
	(4, 'CARNET DE CONDUCIR', '', 0);
/*!40000 ALTER TABLE `as_tipo_documento` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_usuarios
DROP TABLE IF EXISTS `as_usuarios`;
CREATE TABLE IF NOT EXISTS `as_usuarios` (
  `codigo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_completo` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre_usuario` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `email_usuario` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `password_usuario` text COLLATE utf8_spanish2_ci NOT NULL,
  `rol_usuario` int(11) NOT NULL,
  `estado_usuario` int(1) NOT NULL,
  `imagen_usuario` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `status_usuario` int(1) NOT NULL DEFAULT '0',
  `fecha_creacion` datetime NOT NULL,
  `enlace_recuperar_pass` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `fecha_cambio` datetime DEFAULT NULL,
  `status_cambio` int(1) DEFAULT NULL,
  PRIMARY KEY (`codigo_usuario`),
  KEY `fk_rol_usuario` (`rol_usuario`),
  CONSTRAINT `fk_rol_usuario` FOREIGN KEY (`rol_usuario`) REFERENCES `as_roles` (`codigo_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_usuarios: ~6 rows (aproximadamente)
DELETE FROM `as_usuarios`;
/*!40000 ALTER TABLE `as_usuarios` DISABLE KEYS */;
INSERT INTO `as_usuarios` (`codigo_usuario`, `nombre_completo`, `nombre_usuario`, `email_usuario`, `password_usuario`, `rol_usuario`, `estado_usuario`, `imagen_usuario`, `status_usuario`, `fecha_creacion`, `enlace_recuperar_pass`, `fecha_cambio`, `status_cambio`) VALUES
	(17, 'Maria Jhose Garcia Munguia', 'MARIAGARCIA', 'marygmunguia@gmail.com', 'U2FuZHJhPzE5Nzg=', 2, 0, 'views\\img\\default-150x150.png', 0, '2022-06-01 22:16:01', NULL, '2022-06-26 10:00:15', 0);
INSERT INTO `as_usuarios` (`codigo_usuario`, `nombre_completo`, `nombre_usuario`, `email_usuario`, `password_usuario`, `rol_usuario`, `estado_usuario`, `imagen_usuario`, `status_usuario`, `fecha_creacion`, `enlace_recuperar_pass`, `fecha_cambio`, `status_cambio`) VALUES
	(18, 'Rosa Gonzales', 'ROSA01', 'rosagonzales@gmail.com', 'MTIzNDU2Nzg5', 9, 0, 'views\\img\\default-150x150.png', 0, '2022-06-02 16:02:33', NULL, '2022-06-12 13:08:57', 0);
INSERT INTO `as_usuarios` (`codigo_usuario`, `nombre_completo`, `nombre_usuario`, `email_usuario`, `password_usuario`, `rol_usuario`, `estado_usuario`, `imagen_usuario`, `status_usuario`, `fecha_creacion`, `enlace_recuperar_pass`, `fecha_cambio`, `status_cambio`) VALUES
	(19, 'Carlota Josefina Dominguez', 'JOSEFINA100', 'carlota100@gmail.com', 'MTIzNDU2Nzg5', 7, 0, 'views\\img\\default-150x150.png', 0, '2022-06-03 20:04:19', NULL, '2022-06-12 12:46:47', 0);
INSERT INTO `as_usuarios` (`codigo_usuario`, `nombre_completo`, `nombre_usuario`, `email_usuario`, `password_usuario`, `rol_usuario`, `estado_usuario`, `imagen_usuario`, `status_usuario`, `fecha_creacion`, `enlace_recuperar_pass`, `fecha_cambio`, `status_cambio`) VALUES
	(20, 'Juan Mendez Munguia', 'JUAN01', 'juan@gmail.com', 'U2FuZHJhXzE5Nzg=', 5, 0, 'views\\img\\default-150x150.png', 0, '2022-06-05 11:19:10', NULL, '2022-06-05 11:20:51', 0);
INSERT INTO `as_usuarios` (`codigo_usuario`, `nombre_completo`, `nombre_usuario`, `email_usuario`, `password_usuario`, `rol_usuario`, `estado_usuario`, `imagen_usuario`, `status_usuario`, `fecha_creacion`, `enlace_recuperar_pass`, `fecha_cambio`, `status_cambio`) VALUES
	(21, 'Juana Perez', 'JUANITA', 'juana@gmail.com', 'SG9sYTEyMw==', 7, 0, 'views\\img\\default-150x150.png', 0, '2022-06-05 11:39:05', NULL, NULL, 1);
INSERT INTO `as_usuarios` (`codigo_usuario`, `nombre_completo`, `nombre_usuario`, `email_usuario`, `password_usuario`, `rol_usuario`, `estado_usuario`, `imagen_usuario`, `status_usuario`, `fecha_creacion`, `enlace_recuperar_pass`, `fecha_cambio`, `status_cambio`) VALUES
	(22, 'Mario Hernandez', 'MARIO', 'mario@gmail.com', 'NDY2NjUyMTA1NzQ3', 5, 0, 'views\\img\\default-150x150.png', 0, '2022-06-07 12:11:00', NULL, '2022-06-07 12:16:18', 0);
/*!40000 ALTER TABLE `as_usuarios` ENABLE KEYS */;

-- Volcando estructura para tabla aluvir.as_usuario_preguntas
DROP TABLE IF EXISTS `as_usuario_preguntas`;
CREATE TABLE IF NOT EXISTS `as_usuario_preguntas` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_pregunta` int(11) NOT NULL,
  `codigo_usuario` int(11) NOT NULL,
  `respuesta` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_pregunta_ups` (`codigo_pregunta`),
  KEY `fk_usuario_ups` (`codigo_usuario`),
  CONSTRAINT `fk_pregunta_ups` FOREIGN KEY (`codigo_pregunta`) REFERENCES `as_preguntas_seguridad` (`codigo_pregunta`),
  CONSTRAINT `fk_usuario_ups` FOREIGN KEY (`codigo_usuario`) REFERENCES `as_usuarios` (`codigo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- Volcando datos para la tabla aluvir.as_usuario_preguntas: ~9 rows (aproximadamente)
DELETE FROM `as_usuario_preguntas`;
/*!40000 ALTER TABLE `as_usuario_preguntas` DISABLE KEYS */;
INSERT INTO `as_usuario_preguntas` (`codigo`, `codigo_pregunta`, `codigo_usuario`, `respuesta`) VALUES
	(10, 1, 18, 'POPI');
INSERT INTO `as_usuario_preguntas` (`codigo`, `codigo_pregunta`, `codigo_usuario`, `respuesta`) VALUES
	(11, 2, 18, 'OLANCHITO');
INSERT INTO `as_usuario_preguntas` (`codigo`, `codigo_pregunta`, `codigo_usuario`, `respuesta`) VALUES
	(12, 3, 18, 'ALFREDO');
INSERT INTO `as_usuario_preguntas` (`codigo`, `codigo_pregunta`, `codigo_usuario`, `respuesta`) VALUES
	(16, 1, 20, 'popi');
INSERT INTO `as_usuario_preguntas` (`codigo`, `codigo_pregunta`, `codigo_usuario`, `respuesta`) VALUES
	(17, 2, 20, 'olanchito');
INSERT INTO `as_usuario_preguntas` (`codigo`, `codigo_pregunta`, `codigo_usuario`, `respuesta`) VALUES
	(18, 3, 20, 'alfredo');
INSERT INTO `as_usuario_preguntas` (`codigo`, `codigo_pregunta`, `codigo_usuario`, `respuesta`) VALUES
	(19, 1, 17, 'POPI');
INSERT INTO `as_usuario_preguntas` (`codigo`, `codigo_pregunta`, `codigo_usuario`, `respuesta`) VALUES
	(20, 2, 17, 'OLANCHITO');
INSERT INTO `as_usuario_preguntas` (`codigo`, `codigo_pregunta`, `codigo_usuario`, `respuesta`) VALUES
	(21, 3, 17, 'ALFREDO');
/*!40000 ALTER TABLE `as_usuario_preguntas` ENABLE KEYS */;

-- Volcando estructura para vista aluvir.as_mostrar_bitacora
DROP VIEW IF EXISTS `as_mostrar_bitacora`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_mostrar_bitacora` (
	`identificador` INT(11) NOT NULL,
	`codigo_registro` INT(11) NOT NULL,
	`tabla_registro` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`accion_registro` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`campo_registro` VARCHAR(50) NULL COLLATE 'utf8_spanish2_ci',
	`valor_anterior` TEXT NULL COLLATE 'utf8_spanish2_ci',
	`valor_actual` TEXT NULL COLLATE 'utf8_spanish2_ci',
	`fecha` DATE NOT NULL,
	`hora` TIME NOT NULL,
	`nombre_completo` VARCHAR(60) NOT NULL COLLATE 'utf8_spanish2_ci'
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_mostrar_clientes
DROP VIEW IF EXISTS `as_mostrar_clientes`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_mostrar_clientes` (
	`codigo_cliente` INT(11) NOT NULL,
	`tipo_identificacion` INT(11) NULL,
	`identificacion` VARCHAR(20) NULL COLLATE 'utf8_spanish2_ci',
	`nombre` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`apellido` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`fecha_nacimiento` DATE NULL,
	`sexo` ENUM('FEMENINO','MASCULINO','NO DEFINIDO') NULL COLLATE 'utf8_spanish2_ci',
	`email` VARCHAR(30) NULL COLLATE 'utf8_spanish2_ci',
	`telefono` VARCHAR(15) NULL COLLATE 'utf8_spanish2_ci',
	`celular` VARCHAR(15) NULL COLLATE 'utf8_spanish2_ci',
	`credito` INT(1) NOT NULL,
	`limite_credito` DOUBLE(17,2) NULL,
	`codigo_documento` INT(11) NOT NULL,
	`nombre_documento` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`descripcion_documento` TEXT NULL COLLATE 'utf8_spanish2_ci'
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_mostrar_compras
DROP VIEW IF EXISTS `as_mostrar_compras`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_mostrar_compras` (
	`codigo_proveedor` INT(11) NOT NULL,
	`rtn_proveedor` VARCHAR(20) NULL COLLATE 'utf8_spanish2_ci',
	`nombre_proveedor` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`direccion_proveedor` VARCHAR(255) NULL COLLATE 'utf8_spanish2_ci',
	`telefono_proveedor` VARCHAR(10) NULL COLLATE 'utf8_spanish2_ci',
	`celular_proveedor` VARCHAR(10) NULL COLLATE 'utf8_spanish2_ci',
	`email_proveedor` VARCHAR(50) NULL COLLATE 'utf8_spanish2_ci',
	`nombre_contacto` VARCHAR(50) NULL COLLATE 'utf8_spanish2_ci',
	`celular_contacto` VARCHAR(10) NULL COLLATE 'utf8_spanish2_ci',
	`email_contacto` VARCHAR(50) NULL COLLATE 'utf8_spanish2_ci',
	`codigo_compra` INT(11) NOT NULL,
	`no_factura` VARCHAR(30) NULL COLLATE 'utf8_spanish2_ci',
	`proveedor` INT(11) NOT NULL,
	`fecha` DATE NOT NULL,
	`subtotal` DECIMAL(20,2) NOT NULL,
	`impuestos` DECIMAL(20,2) NOT NULL,
	`descuento` DECIMAL(20,2) NULL,
	`total` DECIMAL(20,2) NOT NULL,
	`materiales` TEXT NULL COLLATE 'utf8_spanish2_ci'
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_mostrar_direcciones
DROP VIEW IF EXISTS `as_mostrar_direcciones`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_mostrar_direcciones` (
	`codigo_direccion` INT(11) NOT NULL,
	`departamento` VARCHAR(50) NULL COLLATE 'utf8_spanish2_ci',
	`municipio` VARCHAR(50) NULL COLLATE 'utf8_spanish2_ci',
	`ciudad` VARCHAR(50) NULL COLLATE 'utf8_spanish2_ci',
	`barrio_colonia` VARCHAR(60) NULL COLLATE 'utf8_spanish2_ci',
	`detalle_direccion` VARCHAR(255) NULL COLLATE 'utf8_spanish2_ci',
	`codigo_cliente` INT(11) NOT NULL,
	`nombre` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`apellido` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci'
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_mostrar_empleados
DROP VIEW IF EXISTS `as_mostrar_empleados`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_mostrar_empleados` (
	`codigo_empleado` INT(11) NOT NULL,
	`tipo_documento` INT(11) NOT NULL,
	`no_documento` VARCHAR(25) NULL COLLATE 'utf8_spanish2_ci',
	`nombre` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`apellido` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`fecha_nacimiento` DATE NULL,
	`sexo` ENUM('FEMENINO','MASCULINO','NO DEFINIDO') NULL COLLATE 'utf8_spanish2_ci',
	`email` VARCHAR(50) NULL COLLATE 'utf8_spanish2_ci',
	`telefono` VARCHAR(10) NULL COLLATE 'utf8_spanish2_ci',
	`celular` VARCHAR(10) NULL COLLATE 'utf8_spanish2_ci',
	`codigo_documento` INT(11) NOT NULL,
	`nombre_documento` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`descripcion_documento` TEXT NULL COLLATE 'utf8_spanish2_ci',
	`status_documento` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_mostrar_materiales
DROP VIEW IF EXISTS `as_mostrar_materiales`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_mostrar_materiales` (
	`codigo_material` INT(11) NOT NULL,
	`codigo_barras` VARCHAR(30) NULL COLLATE 'utf8_spanish2_ci',
	`nombre_material` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`categoria` INT(11) NOT NULL,
	`detalle_material` TEXT NULL COLLATE 'utf8_spanish2_ci',
	`precio_venta` DECIMAL(20,2) UNSIGNED NOT NULL,
	`existencias` INT(11) NOT NULL,
	`existencias_minimas` INT(11) NOT NULL,
	`existencias_maximas` INT(11) NOT NULL,
	`codigo_categoria` INT(11) NOT NULL,
	`nombre_categoria` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`descripcion_categoria` VARCHAR(255) NULL COLLATE 'utf8_spanish2_ci'
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_mostrar_productos
DROP VIEW IF EXISTS `as_mostrar_productos`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_mostrar_productos` (
	`codigo_producto` INT(11) NOT NULL,
	`nombre_producto` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`detalle_producto` VARCHAR(255) NULL COLLATE 'utf8_spanish2_ci',
	`categoria` INT(11) NOT NULL,
	`precio_venta` DECIMAL(20,2) NOT NULL,
	`existencias` INT(11) NULL,
	`existencias_iniciales` INT(11) NULL,
	`materiales_producto` TEXT NULL COLLATE 'utf8_spanish2_ci',
	`codigo_categoria` INT(11) NOT NULL,
	`nombre_categoria` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`descripcion_categoria` VARCHAR(255) NULL COLLATE 'utf8_spanish2_ci',
	`status_categoria` INT(1) NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_ultima_compra_registrada
DROP VIEW IF EXISTS `as_ultima_compra_registrada`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_ultima_compra_registrada` (
	`codigo_compra` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_ultimo_producto_registrado
DROP VIEW IF EXISTS `as_ultimo_producto_registrado`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_ultimo_producto_registrado` (
	`codigo_producto` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_vw_acciones_permisos_secciones
DROP VIEW IF EXISTS `as_vw_acciones_permisos_secciones`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_vw_acciones_permisos_secciones` (
	`codigo_permiso` INT(11) NOT NULL,
	`codigo_accion` INT(11) NOT NULL,
	`nombre_accion` VARCHAR(50) NULL COLLATE 'utf8_spanish2_ci',
	`codigo_seccion` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_vw_consultar_permisos
DROP VIEW IF EXISTS `as_vw_consultar_permisos`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_vw_consultar_permisos` (
	`codigo_usuario` INT(11) NOT NULL,
	`codigo_rol` INT(11) NOT NULL,
	`codigo_permiso` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_vw_ultimo_rol_creado
DROP VIEW IF EXISTS `as_vw_ultimo_rol_creado`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_vw_ultimo_rol_creado` (
	`codigo_rol` INT(11) NOT NULL,
	`nombre_rol` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`descripcion_rol` TEXT NOT NULL COLLATE 'utf8_spanish2_ci',
	`status_registro` INT(1) NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_vw_ultimo_usuario_creado
DROP VIEW IF EXISTS `as_vw_ultimo_usuario_creado`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_vw_ultimo_usuario_creado` (
	`codigo_usuario` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_vw_usuarios
DROP VIEW IF EXISTS `as_vw_usuarios`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_vw_usuarios` (
	`codigo_usuario` INT(11) NOT NULL,
	`nombre_completo` VARCHAR(60) NOT NULL COLLATE 'utf8_spanish2_ci',
	`nombre_usuario` VARCHAR(25) NOT NULL COLLATE 'utf8_spanish2_ci',
	`email_usuario` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`password_usuario` TEXT NOT NULL COLLATE 'utf8_spanish2_ci',
	`rol_usuario` INT(11) NOT NULL,
	`estado_usuario` INT(1) NOT NULL,
	`imagen_usuario` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`status_usuario` INT(1) NOT NULL,
	`fecha_creacion` DATETIME NOT NULL,
	`enlace_recuperar_pass` VARCHAR(50) NULL COLLATE 'utf8_spanish2_ci',
	`fecha_cambio` DATETIME NULL,
	`status_cambio` INT(1) NULL
) ENGINE=MyISAM;

-- Volcando estructura para vista aluvir.as_vw_usuarios_preguntas
DROP VIEW IF EXISTS `as_vw_usuarios_preguntas`;
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `as_vw_usuarios_preguntas` (
	`codigo_usuario` INT(11) NOT NULL,
	`nombre_usuario` VARCHAR(25) NOT NULL COLLATE 'utf8_spanish2_ci',
	`email_usuario` VARCHAR(50) NOT NULL COLLATE 'utf8_spanish2_ci',
	`cuerpo_pregunta` VARCHAR(255) NOT NULL COLLATE 'utf8_spanish2_ci',
	`respuesta` VARCHAR(25) NOT NULL COLLATE 'utf8_spanish2_ci'
) ENGINE=MyISAM;

-- Volcando estructura para procedimiento aluvir.as_sp_eliminar_cliente
DROP PROCEDURE IF EXISTS `as_sp_eliminar_cliente`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `as_sp_eliminar_cliente`(
	IN `codigo` INT
)
BEGIN
DELETE FROM as_clientes WHERE codigo_cliente = codigo;
END//
DELIMITER ;

-- Volcando estructura para procedimiento aluvir.as_sp_eliminar_preguntas
DROP PROCEDURE IF EXISTS `as_sp_eliminar_preguntas`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `as_sp_eliminar_preguntas`(
	IN `usuario` INT
)
BEGIN
DELETE FROM as_usuario_preguntas WHERE codigo_usuario = usuario;
END//
DELIMITER ;

-- Volcando estructura para procedimiento aluvir.as_sp_guardar_permiso_usuario
DROP PROCEDURE IF EXISTS `as_sp_guardar_permiso_usuario`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `as_sp_guardar_permiso_usuario`(
	IN `usuario` INT,
	IN `permiso` INT
)
BEGIN
INSERT INTO as_usuarios_permisos VALUES (NULL, usuario, permiso);
END//
DELIMITER ;

-- Volcando estructura para procedimiento aluvir.as_sp_guardar_preguntas
DROP PROCEDURE IF EXISTS `as_sp_guardar_preguntas`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `as_sp_guardar_preguntas`(
	IN `pregunta` INT,
	IN `usuario` INT,
	IN `respuesta` TEXT
)
BEGIN
INSERT INTO as_usuario_preguntas VALUES (NULL, pregunta, usuario, respuesta);
END//
DELIMITER ;

-- Volcando estructura para procedimiento aluvir.as_sp_guardar_usuario
DROP PROCEDURE IF EXISTS `as_sp_guardar_usuario`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `as_sp_guardar_usuario`(
	IN `nombre` VARCHAR(80),
	IN `usuario` VARCHAR(25),
	IN `email` VARCHAR(50),
	IN `pass` TEXT,
	IN `rol` INT,
	IN `estado` INT,
	IN `imagen` VARCHAR(50),
	IN `creacion` DATETIME
)
    DETERMINISTIC
BEGIN
	INSERT INTO as_usuarios VALUES (NULL, nombre, usuario, email, pass, rol, estado, imagen, 0, creacion, NULL, NULL, 1);
END//
DELIMITER ;

-- Volcando estructura para disparador aluvir.as_trigger_borrar_permisos
DROP TRIGGER IF EXISTS `as_trigger_borrar_permisos`;
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE DEFINER=`root`@`localhost` TRIGGER `as_trigger_borrar_permisos` AFTER UPDATE ON `as_roles` FOR EACH ROW BEGIN
DELETE FROM as_roles_permiso WHERE codigo_rol = OLD.codigo_rol;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Volcando estructura para vista aluvir.as_mostrar_bitacora
DROP VIEW IF EXISTS `as_mostrar_bitacora`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_mostrar_bitacora`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_mostrar_bitacora` AS SELECT b.identificador, b.codigo_registro, b.tabla_registro, b.accion_registro, b.campo_registro,
b.valor_anterior, b.valor_actual, b.fecha, b.hora, u.nombre_completo FROM as_bitacora b, as_usuarios u WHERE u.codigo_usuario = b.codigo_usuario ;

-- Volcando estructura para vista aluvir.as_mostrar_clientes
DROP VIEW IF EXISTS `as_mostrar_clientes`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_mostrar_clientes`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_mostrar_clientes` AS SELECT * FROM as_clientes c, as_tipo_documento d WHERE c.tipo_identificacion = d.codigo_documento ;

-- Volcando estructura para vista aluvir.as_mostrar_compras
DROP VIEW IF EXISTS `as_mostrar_compras`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_mostrar_compras`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_mostrar_compras` AS SELECT * FROM  as_proveedores p, as_compras c WHERE c.proveedor = p.codigo_proveedor ;

-- Volcando estructura para vista aluvir.as_mostrar_direcciones
DROP VIEW IF EXISTS `as_mostrar_direcciones`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_mostrar_direcciones`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_mostrar_direcciones` AS SELECT d.codigo_direccion, d.departamento, d.municipio, d.ciudad, d.barrio_colonia, d.detalle_direccion,
c.codigo_cliente, c.nombre, c.apellido FROM as_clientes c, as_direcciones d WHERE c.codigo_cliente = d.codigo_cliente ORDER BY c.codigo_cliente ;

-- Volcando estructura para vista aluvir.as_mostrar_empleados
DROP VIEW IF EXISTS `as_mostrar_empleados`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_mostrar_empleados`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_mostrar_empleados` AS SELECT * FROM as_empleados e, as_tipo_documento td WHERE e.tipo_documento = td.codigo_documento ;

-- Volcando estructura para vista aluvir.as_mostrar_materiales
DROP VIEW IF EXISTS `as_mostrar_materiales`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_mostrar_materiales`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_mostrar_materiales` AS SELECT * FROM as_materiales m, as_categorias c WHERE m.categoria = c.codigo_categoria ;

-- Volcando estructura para vista aluvir.as_mostrar_productos
DROP VIEW IF EXISTS `as_mostrar_productos`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_mostrar_productos`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_mostrar_productos` AS SELECT * FROM as_productos p, as_categorias c WHERE p.categoria = c.codigo_categoria ;

-- Volcando estructura para vista aluvir.as_ultima_compra_registrada
DROP VIEW IF EXISTS `as_ultima_compra_registrada`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_ultima_compra_registrada`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_ultima_compra_registrada` AS SELECT c.codigo_compra FROM as_compras c ORDER BY c.codigo_compra DESC LIMIT 1 ;

-- Volcando estructura para vista aluvir.as_ultimo_producto_registrado
DROP VIEW IF EXISTS `as_ultimo_producto_registrado`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_ultimo_producto_registrado`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_ultimo_producto_registrado` AS SELECT p.codigo_producto FROM as_productos p ORDER BY p.codigo_producto DESC LIMIT 1 ;

-- Volcando estructura para vista aluvir.as_vw_acciones_permisos_secciones
DROP VIEW IF EXISTS `as_vw_acciones_permisos_secciones`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_vw_acciones_permisos_secciones`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_vw_acciones_permisos_secciones` AS SELECT p.codigo_permiso, a.codigo_accion, a.nombre_accion, s.codigo_seccion FROM as_permisos p, as_acciones a, as_secciones s WHERE
p.codigo_accion = a.codigo_accion AND p.codigo_seccion = s.codigo_seccion ORDER BY a.codigo_accion ASC ;

-- Volcando estructura para vista aluvir.as_vw_consultar_permisos
DROP VIEW IF EXISTS `as_vw_consultar_permisos`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_vw_consultar_permisos`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_vw_consultar_permisos` AS SELECT u.codigo_usuario, r.codigo_rol, rp.codigo_permiso 
FROM as_roles r, as_roles_permiso rp, as_usuarios u WHERE u.rol_usuario = rp.codigo_rol AND rp.codigo_rol = r.codigo_rol ;

-- Volcando estructura para vista aluvir.as_vw_ultimo_rol_creado
DROP VIEW IF EXISTS `as_vw_ultimo_rol_creado`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_vw_ultimo_rol_creado`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_vw_ultimo_rol_creado` AS SELECT * FROM as_roles ORDER BY codigo_rol DESC LIMIT 1 ;

-- Volcando estructura para vista aluvir.as_vw_ultimo_usuario_creado
DROP VIEW IF EXISTS `as_vw_ultimo_usuario_creado`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_vw_ultimo_usuario_creado`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_vw_ultimo_usuario_creado` AS SELECT (codigo_usuario) from as_usuarios ORDER BY fecha_creacion DESC LIMIT 1 ;

-- Volcando estructura para vista aluvir.as_vw_usuarios
DROP VIEW IF EXISTS `as_vw_usuarios`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_vw_usuarios`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_vw_usuarios` AS SELECT * FROM as_usuarios WHERE status_usuario = 0 ;

-- Volcando estructura para vista aluvir.as_vw_usuarios_preguntas
DROP VIEW IF EXISTS `as_vw_usuarios_preguntas`;
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `as_vw_usuarios_preguntas`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `as_vw_usuarios_preguntas` AS SELECT u.codigo_usuario, u.nombre_usuario, u.email_usuario , p.cuerpo_pregunta, up.respuesta FROM as_usuarios u, as_preguntas_seguridad p, as_usuario_preguntas up WHERE
u.codigo_usuario = up.codigo_usuario AND up.codigo_pregunta = p.codigo_pregunta ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
